import './App.css';
import {useEffect, useState} from 'react';

 function App() {
  const [forecasts, setForecasts] = useState([]);

  useEffect(() => {
    fetch('http://localhost:5000/WeatherForecast')
    .then((response) => response.json())
    .then((result) => {
      setForecasts(result);
    })
  }, []);


  return (
    <table>
    <thead>
      <tr>
        <th>Date</th>
        <th>Temp. (C)</th>
        <th>Temp. (F)</th>
        <th>Summary</th>
      </tr>
    </thead>
    <tbody>
      {forecasts.map(forecast =>
        <tr key={forecast.date}>
          <td>{forecast.date}</td>
          <td>{forecast.temperatureC}</td>
          <td>{forecast.temperatureF}</td>
          <td>{forecast.summary}</td>
        </tr>
      )}
    </tbody>
  </table>
  );
}

export default App;